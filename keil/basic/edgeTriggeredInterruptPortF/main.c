//#include <tm4c123gh6pm.h>
// user button connected to PF4 (increment counter on falling edge)

#define NVIC_EN0_R              (*((volatile unsigned long *)0xE000E100))  // IRQ 0 to 31 Set Enable Register
#define NVIC_PRI7_R             (*((volatile unsigned long *)0xE000E41C))  // IRQ 28 to 31 Priority Register
#define GPIO_PORTF_DIR_R        (*((volatile unsigned long *)0x40025400))
#define GPIO_PORTF_IS_R         (*((volatile unsigned long *)0x40025404))
#define GPIO_PORTF_IBE_R        (*((volatile unsigned long *)0x40025408))
#define GPIO_PORTF_IEV_R        (*((volatile unsigned long *)0x4002540C))
#define GPIO_PORTF_IM_R         (*((volatile unsigned long *)0x40025410))
#define GPIO_PORTF_RIS_R        (*((volatile unsigned long *)0x40025414))
#define GPIO_PORTF_ICR_R        (*((volatile unsigned long *)0x4002541C))
#define GPIO_PORTF_AFSEL_R      (*((volatile unsigned long *)0x40025420))
#define GPIO_PORTF_PUR_R        (*((volatile unsigned long *)0x40025510))
#define GPIO_PORTF_PDR_R        (*((volatile unsigned long *)0x40025514))
#define GPIO_PORTF_DEN_R        (*((volatile unsigned long *)0x4002551C))
#define GPIO_PORTF_AMSEL_R      (*((volatile unsigned long *)0x40025528))
#define GPIO_PORTF_PCTL_R       (*((volatile unsigned long *)0x4002552C))
#define SYSCTL_RCGC2_R          (*((volatile unsigned long *)0x400FE108))
#define SYSCTL_RCGC2_GPIOF      0x00000020  // port F Clock Gating Control

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode

void SystemInit(void) {
}

// global variable visible in Watch window of debugger
// increments at least once per button press
volatile unsigned long FallingEdges = 0;
#define PF4_MASK  0x10
#define PF0_MASK  0x01
void initializePortF4(void) {
  GPIO_PORTF_DIR_R &= ~PF4_MASK;    // (c) make PF4 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~PF4_MASK;  //     disable alt funct on PF4
  GPIO_PORTF_DEN_R |= PF4_MASK;     //     enable digital I/O on PF4   
  GPIO_PORTF_PCTL_R &= ~0x000F0000; // configure PF4 as GPIO
  GPIO_PORTF_AMSEL_R = 0;       //     disable analog functionality on PF
  GPIO_PORTF_PUR_R |= PF4_MASK;     //     enable weak pull-up on PF4
  GPIO_PORTF_IS_R &= ~PF4_MASK;     // (d) PF4 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~PF4_MASK;    //     PF4 is not both edges
  GPIO_PORTF_IEV_R &= ~PF4_MASK;    //     PF4 falling edge event
  GPIO_PORTF_ICR_R = PF4_MASK;      // (e) clear flag4
  GPIO_PORTF_IM_R |= PF4_MASK;      // (f) arm interrupt on PF4
}
void initializePortF0(void) {
  GPIO_PORTF_DIR_R &= ~PF0_MASK;    // (c) make PF4 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~PF0_MASK;  //     disable alt funct on PF4
  GPIO_PORTF_DEN_R |= PF0_MASK;     //     enable digital I/O on PF4   
 // GPIO_PORTF_PCTL_R &= ~0x000F0000; // configure PF4 as GPIO
	GPIO_PORTF_PCTL_R &= ~0x0000000F; // configure PF0 as GPIO
  //GPIO_PORTF_PCTL_R &= ~0xFFFFFFFF; // configure PF4 as GPIO
  GPIO_PORTF_AMSEL_R = 0;       //     disable analog functionality on PF
  GPIO_PORTF_PUR_R |= PF0_MASK;     //     enable weak pull-up on PF4
  GPIO_PORTF_IS_R &= ~PF0_MASK;     // (d) PF4 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~PF0_MASK;    //     PF4 is not both edges
  GPIO_PORTF_IEV_R &= ~PF0_MASK;    //     PF4 falling edge event
  GPIO_PORTF_ICR_R = PF0_MASK;      // (e) clear flag4
  GPIO_PORTF_IM_R |= PF0_MASK;      // (f) arm interrupt on PF4
}
void initializePortF(void) {
  SYSCTL_RCGC2_R |= 0x00000020; // (a) activate clock for port F
  FallingEdges = 0;             // (b) initialize counter
	initializePortF0();
	//initializePortF4();
}

void EdgeCounter_Init(void){  
  initializePortF();	
  NVIC_PRI7_R = (NVIC_PRI7_R&0xFF00FFFF)|0x00A00000; // (g) priority 5
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
  EnableInterrupts();           // (i) Clears the I bit
}

void GPIOPortF_Handler(void){
	unsigned long port_status;
	port_status = GPIO_PORTF_RIS_R & PF0_MASK;
	port_status = GPIO_PORTF_RIS_R & PF4_MASK;
	
	if(GPIO_PORTF_RIS_R & 0x10) {
		 // this means it is set - but must use GPIO_PORTF_ICR_R to clear
	}
  GPIO_PORTF_ICR_R = 0x10;      // acknowledge flag4
	port_status = GPIO_PORTF_RIS_R & PF0_MASK;
	port_status = GPIO_PORTF_RIS_R & PF4_MASK;

		if(!(GPIO_PORTF_RIS_R & 0x10)) {
		 // RIS should now be cleared - check here
		 // this can be used to check for which pin caused the interrupt
		 // if multiple pins are set up on PORTF to respond to interrupt
	}

  FallingEdges = FallingEdges + 1;
}

//debug code
int main(void){
  EdgeCounter_Init();           // initialize GPIO Port F interrupt
  while(1){
    WaitForInterrupt();
  }
}
