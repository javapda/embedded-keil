#include <tm4c123gh6pm.h>

#define PIN0 0x01
#define PIN1 0x02
#define PIN_TARGET PIN0


/**********************************************
*  Activate the TM4C with a 16 MHz crystal to run at 80 MHz
*  80 MHz = 12.5 ns per clock tick
**********************************************/
void initializePLL(void) {
	// 0) Use RCC2
  SYSCTL_RCC2_R |=  0x80000000;  // USERCC2
  // 1) bypass PLL while initializing
  SYSCTL_RCC2_R |=  0x00000800;  // BYPASS2, PLL bypass
  // 2) select the crystal value and oscillator source
  SYSCTL_RCC_R = (SYSCTL_RCC_R &~0x000007C0)   // clear XTAL field, bits 10-6
                 + 0x00000540;   // 10101, configure for 16 MHz crystal
  SYSCTL_RCC2_R &= ~0x00000070;  // configure for main oscillator source
  // 3) activate PLL by clearing PWRDN
  SYSCTL_RCC2_R &= ~0x00002000;
  // 4) set the desired system divider
  SYSCTL_RCC2_R |= 0x40000000;   // use 400 MHz PLL
  SYSCTL_RCC2_R = (SYSCTL_RCC2_R&~ 0x1FC00000)  // clear system clock divider
                  + (4<<22);      // configure for 80 MHz clock
  // 5) wait for the PLL to lock by polling PLLLRIS
  while((SYSCTL_RIS_R&0x00000040)==0){};  // wait for PLLRIS bit
  // 6) enable use of PLL by clearing BYPASS
  SYSCTL_RCC2_R &= ~0x00000800;
}
void initializeSysTick(void) {
	NVIC_ST_CTRL_R = 0;               // disable SysTick during setup
  NVIC_ST_CTRL_R = 0x00000005;      // enable SysTick with core clock
}
void initializePORTE() {
	volatile unsigned long delay;
	SYSCTL_RCGC2_R |= 0x00000010;     // enable clock Port E
	delay = SYSCTL_RCGC2_R;
	GPIO_PORTE_LOCK_R = 0x4c4f434b;   // unlock GPIO Port E
	GPIO_PORTE_PCTL_R = 0x00000000;   // clear PCTL
	GPIO_PORTE_CR_R = PIN_TARGET;     // allow changes PE0
	GPIO_PORTE_DIR_R |= PIN_TARGET;       // PE0 output
  GPIO_PORTE_AMSEL_R &= ~PIN_TARGET;    // disable analog on PE0
  GPIO_PORTE_AFSEL_R &= ~PIN_TARGET;    // disable alt funct on PE0
  GPIO_PORTE_DEN_R |= PIN_TARGET;       // enable digital I/O on PE0
	
}

void initialize(void) {
	initializePLL();
	initializeSysTick();
	initializePORTE();
}

// The delay parameter is in units of the 80 MHz core clock. (12.5 ns)
void SysTick_Wait(
	unsigned long delay){
  NVIC_ST_RELOAD_R = delay-1;  // number of counts to wait
  NVIC_ST_CURRENT_R = 0;       // any value written to CURRENT clears
  while((NVIC_ST_CTRL_R&0x00010000)==0){ // wait for count flag
  }
}
// 800000*12.5ns equals 10ms
void SysTick_Wait10ms(unsigned long delay){
  unsigned long i;
  for(i=0; i<delay; i++){
    SysTick_Wait(800000);  // wait 10ms
  }
}
void delaySeconds(unsigned long delaySeconds) {
	unsigned long i;
  for(i=0; i<delaySeconds; i++){
    SysTick_Wait10ms(100);  // wait 1sec
  }
}	
void ledToggle(void) {
	GPIO_PORTE_DATA_R ^=  PIN_TARGET; //0x01;  
	//GPIO_PORTE_DATA_R |= 0x01; 
}
void SystemInit(void) {
}
int main(void) {
	initialize();
	while(1) {
		ledToggle();
		delaySeconds(1);
	}

}
