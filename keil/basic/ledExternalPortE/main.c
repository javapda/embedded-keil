#include <tm4c123gh6pm.h>
void SystemInit(void) {
}
#define PIN0 0x01
#define PIN1 0x02
#define PIN_TARGET PIN0

void initializePORTE() {
	volatile unsigned long delay;
	SYSCTL_RCGC2_R |= 0x00000010;  // enable clock Port E
	delay = SYSCTL_RCGC2_R;
	GPIO_PORTE_LOCK_R = 0x4c4f434b;  // unlock GPIO Port E
	GPIO_PORTE_PCTL_R = 0x00000000;   // clear PCTL
	GPIO_PORTE_CR_R = PIN_TARGET; //0x01;  // allow changes PE0
	GPIO_PORTE_DIR_R |= PIN_TARGET; //0x01;        // PE0 output
  GPIO_PORTE_AMSEL_R &= ~PIN_TARGET; //~0x01;      // disable analog on PE0
  GPIO_PORTE_AFSEL_R &= ~PIN_TARGET; //~0x01;      // disable alt funct on PE0
  GPIO_PORTE_DEN_R |= PIN_TARGET; //0x01;         // enable digital I/O on PE0
	
}


void delayOneSecond(void) {
	volatile unsigned long time;
	time = 145448*10; // 0.1 sec * 10 = 1 sec
	while(time) {
		time--;
	}
}

void ledToggle(void) {
	GPIO_PORTE_DATA_R ^=  PIN_TARGET; //0x01;  
	//GPIO_PORTE_DATA_R |= 0x01; 
}

int main(void) {
	initializePORTE();
	
	while(1) {
		ledToggle();
		delayOneSecond();
	}
  
}
