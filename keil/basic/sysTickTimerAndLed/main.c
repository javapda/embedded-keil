#include <tm4c123gh6pm.h>
/**
note: at 16MHz, each tick is 62.5 ns [nano-seconds]
*/

#define PERIOD_NS 62.5
#define PIN0 0x01
#define PIN1 0x02
#define PIN_TARGET PIN0
#define NUM_SECONDS_DELAY 1
#define NUM_DATA_POINTS 50

// prototypes
void delayMilliSeconds(unsigned long delayMS);
void delayNanoSeconds(unsigned long delayNS);
void delaySeconds(unsigned long delaySEC);
void ledToggle(void);

// variables
// first data point is wrong, the other 49 will be correct
unsigned long Time[NUM_DATA_POINTS];
unsigned long Data[NUM_DATA_POINTS];

void SystemInit(void) {
}

void delaySeconds(unsigned long delaySEC) {
	unsigned long i;
	for ( i = 0; i < delaySEC; i++ ) {
		delayMilliSeconds(1000);
	}
}
void delayMilliSeconds(unsigned long delayMS) {
	unsigned long i;
	for ( i = 0; i < delayMS; i++ ) {
		delayNanoSeconds(1000);//000);
	}
}
void delayNanoSeconds(unsigned long delayNS) {
	volatile unsigned long now;  // 24-bit time at this call (12.5ns)
	volatile unsigned long start;  // 24-bit time at start of call (12.5ns)
  volatile unsigned long elapsed;  // 24-bit time between calls (12.5ns)
  start = NVIC_ST_CURRENT_R;
	do {
		now = NVIC_ST_CURRENT_R;
		elapsed = (start - now)&0x00FFFFFF; 
		
	} while(elapsed < (delayNS*12.5));
	elapsed=0;
}
void ledToggle(void) {
	GPIO_PORTE_DATA_R ^=  PIN_TARGET; //0x01;  
	//GPIO_PORTE_DATA_R |= 0x01; 
}

void initializeSysTick(void) {
	// setup systick
	// disable systick (turn it off, 0 to Enable bit 0)
	NVIC_ST_CTRL_R &= ~0x01;
	
	// set RELOAD to some init value (limit to 24-bits: 0xFFFFFF)
	NVIC_ST_RELOAD_R = 0x00FFFFFF;
	
	// write any value to the CURRENT register
	NVIC_ST_CURRENT_R = 0x00;
	
	// enable clock (1) [16MHz], disable interrupts (0), enable systick to start it
	NVIC_ST_CTRL_R = 0x00000005;
}

void initializePORTE() {
	volatile unsigned long delay;
	SYSCTL_RCGC2_R |= 0x00000010;  // enable clock Port E
	delay = SYSCTL_RCGC2_R;
	GPIO_PORTE_LOCK_R = 0x4c4f434b;  // unlock GPIO Port E
	GPIO_PORTE_PCTL_R = 0x00000000;   // clear PCTL
	GPIO_PORTE_CR_R = PIN_TARGET; //0x01;  // allow changes PE0
	GPIO_PORTE_DIR_R |= PIN_TARGET; //0x01;        // PE0 output
  GPIO_PORTE_AMSEL_R &= ~PIN_TARGET; //~0x01;      // disable analog on PE0
  GPIO_PORTE_AFSEL_R &= ~PIN_TARGET; //~0x01;      // disable alt funct on PE0
  GPIO_PORTE_DEN_R |= PIN_TARGET; //0x01;         // enable digital I/O on PE0
	
}


int main(void){
	unsigned long i,last,now;
	initializeSysTick();
	initializePORTE();
	i = 0;
	
	while(1) {
		ledToggle();
		if(i<NUM_DATA_POINTS) {
			
      now = NVIC_ST_CURRENT_R;
      Time[i] = (last-now)&0x00FFFFFF;  // 24-bit time difference
      Data[i] = GPIO_PORTE_DATA_R&PIN_TARGET; // record PE
      last = now;
      i++;
    
		}
		delaySeconds(NUM_SECONDS_DELAY);
	}
}
