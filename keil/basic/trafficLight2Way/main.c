#include <tm4c123gh6pm.h>
#include "systickTimer.h"
#include "delay.h"

#define PIN0 0x01
#define PIN1 0x02
#define PIN_TARGET PIN0
#define DELAY_SECONDS 1

#define SENSOR  (*((volatile unsigned long *)0x4002400C))
#define LIGHT   (*((volatile unsigned long *)0x400050FC))

// Linked data structure
struct State {
  unsigned long Out; 
  unsigned long Time;  
  unsigned long Next[4];}; 
typedef const struct State STyp;
#define goN   0
#define waitN 1
#define goE   2
#define waitE 3
STyp FSM[4]={
 {0x21,3000,{goN,waitN,goN,waitN}},  // north - go
 {0x22, 500,{goE,goE,goE,goE}},      // north - yellow
 {0x0C,3000,{goE,goE,waitE,waitE}},
 {0x14, 500,{goN,goN,goN,goN}}};
unsigned long S;  // index to the current state 
unsigned long Input; 
void looper(void);
 
/*
all input
PE1 - north switch
PE0 - east switch
*/
void initializePORTE() {
}
/*
all output
PE5 - east red
PE4 - east yellow
PE3 - east green
PE2 - north red
PE1 - north yellow
PE0 - north green
*/
void initializePORTB() {
}

void initializePORTE_LEDONLY() {
	volatile unsigned long delay;
	SYSCTL_RCGC2_R |= 0x00000010;     // enable clock Port E
	delay = SYSCTL_RCGC2_R;
	GPIO_PORTE_LOCK_R = 0x4c4f434b;   // unlock GPIO Port E
	GPIO_PORTE_PCTL_R = 0x00000000;   // clear PCTL
	GPIO_PORTE_CR_R = PIN_TARGET;     // allow changes PE0
	GPIO_PORTE_DIR_R |= PIN_TARGET;       // PE0 output
  GPIO_PORTE_AMSEL_R &= ~PIN_TARGET;    // disable analog on PE0
  GPIO_PORTE_AFSEL_R &= ~PIN_TARGET;    // disable alt funct on PE0
  GPIO_PORTE_DEN_R |= PIN_TARGET;       // enable digital I/O on PE0
	
}

void initialize(void) {
	
	initializeSysTick();
	initializePORTE();
}


void ledToggle(void) {
	GPIO_PORTE_DATA_R ^=  PIN_TARGET; //0x01;  
	//GPIO_PORTE_DATA_R |= 0x01; 
}
void SystemInit(void) {
}
void flashingLed(void) {
	while(1) {
		ledToggle();
		delaySeconds(DELAY_SECONDS);
	}
}
void setLights(unsigned long pattern) {
	LIGHT = ((LIGHT >> 6)<<6)+pattern;
}	

void looper(void) {
	  //LIGHT = FSM[S].Out;  // set lights
	  setLights(FSM[S].Out);
	  //delaySeconds(5);
    SysTick_Wait10ms(FSM[S].Time);
    Input = SENSOR;     // read sensors
    S = FSM[S].Next[Input];  

}
void north_red(void) {
	LIGHT = 0x04;
}
void north_yellow(void) {
	LIGHT = 0x02;
}
void north_green(void) {
	LIGHT = 0x01;
}
void east_red(void) {
	LIGHT = 0x04 << 3;
}
void east_yellow(void) {
	LIGHT = 0x02 << 3;
}
void east_green(void) {
	LIGHT = 0x01 << 3;
}
void old(void) {
		east_green();
		delaySeconds(5);
		east_yellow();
		delaySeconds(5);
		east_red();
		delaySeconds(5);
		north_green();
		delaySeconds(5);
		north_yellow();
		delaySeconds(5);
		north_red();
		delaySeconds(5);

}
void cycleLights(void) {
		setLights(FSM[goN].Out);
		delaySeconds(5);
		setLights(FSM[waitN].Out);
		delaySeconds(5);
		setLights(FSM[goE].Out);
		delaySeconds(5);
		setLights(FSM[waitE].Out);
		delaySeconds(5);
}
void trafficLight(void) {
	volatile unsigned long delay;
	//unsigned long counter = 0;
	SYSCTL_RCGC2_R |= 0x12;      // 1) B E
  delay = SYSCTL_RCGC2_R;      // 2) no need to unlock
  GPIO_PORTE_AMSEL_R &= ~0x03; // 3) disable analog function on PE1-0
  GPIO_PORTE_PCTL_R &= ~0x000000FF; // 4) enable regular GPIO
  GPIO_PORTE_DIR_R &= ~0x03;   // 5) inputs on PE1-0
  GPIO_PORTE_AFSEL_R &= ~0x03; // 6) regular function on PE1-0
  GPIO_PORTE_DEN_R |= 0x03;    // 7) enable digital on PE1-0
  GPIO_PORTB_AMSEL_R &= ~0x3F; // 3) disable analog function on PB5-0
  GPIO_PORTB_PCTL_R &= ~0x00FFFFFF; // 4) enable regular GPIO
  GPIO_PORTB_DIR_R |= 0x3F;    // 5) outputs on PB5-0
  GPIO_PORTB_AFSEL_R &= ~0x3F; // 6) regular function on PB5-0
  GPIO_PORTB_DEN_R |= 0x3F;    // 7) enable digital on PB5-0
  S = goN;  
  while(1){

		looper();
		//delaySeconds(5);
		//delay1sec(5);
		//counter++;
		//if(counter==1) {
		//	counter = counter;
		//} else if (counter == 3) {
		//	counter = counter;
		//}
  }
}
int main(void) {
	initialize();
	//flashingLed();
	trafficLight();

}
