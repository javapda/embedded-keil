#ifndef SYSTICK_TIMER_H
#define SYSTICK_TIMER_H

void initializeSysTick(void);
void SysTick_Wait(unsigned long delay);
void SysTick_Wait10ms(unsigned long delay);

#endif  /* SYSTICK_TIMER_H */
