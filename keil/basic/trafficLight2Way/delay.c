#include "delay.h"
#include "systickTimer.h"

void delaySeconds(unsigned long delaySeconds) {
	unsigned long i;
  for(i=0; i<delaySeconds; i++){
    SysTick_Wait10ms(100);  // wait 1sec
  }
}	

