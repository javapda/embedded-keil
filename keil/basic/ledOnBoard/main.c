//#include "tm4c123gh6pm.h"
#include <tm4c123gh6pm.h>
void initializePortF() {
	volatile unsigned long delay;
	SYSCTL_RCGC2_R |= 0x00000020;  // enable clock Port F
	delay = SYSCTL_RCGC2_R;
	GPIO_PORTF_LOCK_R = 0x4c4f434b;  // unlock GPIO Port F
	GPIO_PORTF_CR_R = 0x0E;  // allow changes PF4-1
	GPIO_PORTF_AMSEL_R = 0x00; // disable analog Port F
	GPIO_PORTF_PCTL_R = 0x00000000;  // GPIO PF4-0
	GPIO_PORTF_DIR_R = 0x0E; // PF3-1 out
	GPIO_PORTF_AFSEL_R = 0x00; 
	GPIO_PORTF_DEN_R |= 0x0E; // digital I/O PF4-0
}

void delayOneSecond(void) {
	volatile unsigned long time;
	time = 145448*10; // 0.1 sec
	while(time) {
		time--;
	}
}
void SystemInit(void) {
}
void ledRed(void) {
	GPIO_PORTF_DATA_R |= 0x02; // set bit 0 PE0
}
void ledBlue(void) {
	GPIO_PORTF_DATA_R |= 0x04 ; // set bit 0 PE0
}
void ledGreen(void) {
	GPIO_PORTF_DATA_R |= 0x08; // set bit 0 PE0
}
void ledOff(void) {
	GPIO_PORTF_DATA_R &= ~0x0E; // set bit 0 PE0
}

int main(void) {
	initializePortF();
	while(1) {
		//ledRed(); 
		ledGreen();
		//ledBlue();
		delayOneSecond();
		ledOff();
		delayOneSecond();
	}
	//return 0;
}
