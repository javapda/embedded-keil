// ***** 0. Documentation Section *****
// TableTrafficLight.c for Lab 10
// Runs on LM4F120/TM4C123
// Index implementation of a Moore finite state machine to operate a traffic light.  
// Daniel Valvano, Jonathan Valvano
// January 15, 2016

// east/west red light connected to PB5
// east/west yellow light connected to PB4
// east/west green light connected to PB3
// north/south facing red light connected to PB2
// north/south facing yellow light connected to PB1
// north/south facing green light connected to PB0
// pedestrian detector connected to PE2 (1=pedestrian present)
// north/south car detector connected to PE1 (1=car present)
// east/west car detector connected to PE0 (1=car present)
// "walk" light connected to PF3 (built-in green LED)
// "don't walk" light connected to PF1 (built-in red LED)

// ***** 1. Pre-processor Directives Section *****
#include "TExaS.h"
#include "tm4c123gh6pm.h"
#include "systickTimer.h"
#include <stdbool.h>

// ***** 2. Global Declarations Section *****
struct state {
	char name[5];
	unsigned long outLightsTraffic;
	unsigned long outLightsWalk;
	unsigned long waitMilliSeconds;
	unsigned long next[8];
};
typedef struct state stType;
#define SENSOR  GPIO_PORTE_DATA_R
#define TRAFFIC_LIGHTS GPIO_PORTB_DATA_R
#define WALK_LIGHTS GPIO_PORTF_DATA_R

#define gW 0
#define yW 1
#define gS 2
#define yS 3
#define aR 4
#define walk 5
#define hun1 6
#define huf1 7
#define hun2 8
#define huf2 9
#define hun3 10
#define huf3 11
#define DELAY_MILLIS 1000

stType const fsm[12] = {
	{"gW", 0x0C, 0x02, DELAY_MILLIS, { gW, gW, yW, yW, yW, yW, yW, yW }}, // 0 gW
	{"yW", 0x14, 0x02, DELAY_MILLIS, { gS, gS, gS, gS, aR, aR, aR, gS }}, // 1 yW
	{"gS", 0x21, 0x02, DELAY_MILLIS, { gS, yS, gS, yS, yS, yS, yS, yS }}, // 2 gS
	{"yS", 0x22, 0x02, DELAY_MILLIS, { yS, gW, gW, gW, aR, aR, aR, aR }}, // 3 yS
	{"aR", 0x24, 0x02, DELAY_MILLIS, { gW, gW, gS, gW, walk, walk, walk, walk }}, // 4 aR
	{"walk", 0x24, 0x08, DELAY_MILLIS, { hun1, hun1, hun1, hun1, hun1, hun1, hun1, hun1 }}, // 5 walk
	{"hun1", 0x24, 0x02, DELAY_MILLIS, { huf1, huf1, huf1, huf1, huf1, huf1, huf1, huf1 }}, // 6 hun1
	{"huf1", 0x24, 0x00, DELAY_MILLIS, { hun2, hun2, hun2, hun2, hun2, hun2, hun2, hun2 }}, // 7 huf1
	{"hun2", 0x24, 0x01, DELAY_MILLIS, { huf2, huf2, huf2, huf2, huf2, huf2, huf2, huf2 }}, // 8 hun2
	{"huf2", 0x24, 0x00, DELAY_MILLIS, { hun3, hun3, hun3, hun3, hun3, hun3, hun3, hun3 }}, // 9 huf2
	{"hun3", 0x24, 0x01, DELAY_MILLIS, { huf3, huf3, huf3, huf3, huf3, huf3, huf3, huf3 }}, // 10 hun3
	{"huf3", 0x24, 0x00, DELAY_MILLIS, { aR, gW, gS, gW, walk, gW, gS, gW }}  // 11 huf3
};

// FUNCTION PROTOTYPES: Each subroutine defined
void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts

unsigned long s = gW;
//unsigned long s = walk;

unsigned long input;


// ***** 3. Subroutines Section *****
void setLightsTraffic(unsigned long pattern) {
		TRAFFIC_LIGHTS = ((TRAFFIC_LIGHTS >> 6)<<6)+pattern;
}
void setLightsWalk(unsigned long pattern) {
	// PF3, PF1
	WALK_LIGHTS = pattern;
}
void looper(void) {
	while(1) {
		
	  setLightsTraffic(fsm[s].outLightsTraffic);
	  setLightsWalk(fsm[s].outLightsWalk);
	  delayMilliSeconds(fsm[s].waitMilliSeconds);
    input = SENSOR;     // read sensors
	  s = fsm[s].next[input];
		
	}
}

void doWalk(bool enabled) {
	if(enabled) {
		GPIO_PORTF_DATA_R |= 0x08;
	} else {
		GPIO_PORTF_DATA_R &= ~0x08;
	}
}	
void dontwalk(bool enabled) {
	if(enabled) {
		GPIO_PORTF_DATA_R |= 0x02;
	} else {
		GPIO_PORTF_DATA_R &= ~0x02;
	}
}	
/**
* PF3: Walk - green
* PF1: Don't Walk/Hurry Up - red
*/
void initializePortF(void){ volatile unsigned long delay;
  SYSCTL_RCGC2_R |= 0x00000020;     // 1) F clock
  delay = SYSCTL_RCGC2_R;           // delay   
  GPIO_PORTF_LOCK_R = 0x4C4F434B;   // 2) unlock PortF PF0  
  GPIO_PORTF_CR_R = 0x1F;           // allow changes to PF4-0       
  GPIO_PORTF_AMSEL_R = 0x00;        // 3) disable analog function
  GPIO_PORTF_PCTL_R = 0x00000000;   // 4) GPIO clear bit PCTL  
  GPIO_PORTF_DIR_R = 0x0A;          // 5) PF3,PF1 output - 1010 = 0x0A
  GPIO_PORTF_AFSEL_R = 0x00;        // 6) no alternate function
  GPIO_PORTF_PUR_R = 0x00;          // disable pullup resistors on PORT F       
  GPIO_PORTF_DEN_R = 0x1F;          // 7) enable digital pins PF4-PF0        
}

void initialize(void) {
	volatile unsigned long delay;
	initializeSysTick();
	SYSCTL_RCGC2_R |= 0x12;      // 1) B E
  delay = SYSCTL_RCGC2_R;      // 2) no need to unlock
  GPIO_PORTE_AMSEL_R &= ~0x07; // 3) disable analog function on PE2-0
  GPIO_PORTE_PCTL_R &= ~0x000000FF; // 4) enable regular GPIO
  GPIO_PORTE_DIR_R &= ~0x07;   // 5) inputs on PE2-0
  GPIO_PORTE_AFSEL_R &= ~0x07; // 6) regular function on PE2-0
  GPIO_PORTE_DEN_R |= 0x07;    // 7) enable digital on PE2-0
	
  GPIO_PORTB_AMSEL_R &= ~0x3F; // 3) disable analog function on PB5-0
  GPIO_PORTB_PCTL_R &= ~0x00FFFFFF; // 4) enable regular GPIO
  GPIO_PORTB_DIR_R |= 0x3F;    // 5) outputs on PB5-0
  GPIO_PORTB_AFSEL_R &= ~0x3F; // 6) regular function on PB5-0
  GPIO_PORTB_DEN_R |= 0x3F;    // 7) enable digital on PB5-0
	
	initializePortF();
}

void exercise(void) {
	  while(1){
		input = SENSOR;     // read sensors
		doWalk(true);
		delayMilliSeconds(DELAY_MILLIS);
		doWalk(false);
		delayMilliSeconds(DELAY_MILLIS);
		dontwalk(true);
		delayMilliSeconds(DELAY_MILLIS);
		dontwalk(false);
		delayMilliSeconds(DELAY_MILLIS);
  }

}
void walkTest(void) {
	while(1) {
		input = SENSOR;
	}
}

int main(void){ 
  TExaS_Init(SW_PIN_PE210, LED_PIN_PB543210,ScopeOff); // activate grader and set system clock to 80 MHz
  initialize();
  EnableInterrupts();
	//exercise();
	//walkTest();
	looper();
}

