#ifndef SYSTICK_TIMER_H
#define SYSTICK_TIMER_H

void initializeSysTick(void);
void SysTick_Wait(unsigned long delay);
void SysTick_Wait1ms(unsigned long delay);
void SysTick_Wait10ms(unsigned long delay);
void delayMilliSeconds(unsigned long numMilliSeconds);
#endif  /* SYSTICK_TIMER_H */
