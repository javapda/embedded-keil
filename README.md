# README #


Embedded software for the TM4C123 using Keil uVision V4.74.0.22

### New Keil Project - Build from Scratch ###
Project -> New uVision Project...
(select name [ledOnBoard] and location on filesystem)

Edit options:

* Device: TM4C123GH6PM
* Floating Point Hardware: Not Used (default is Use FPU)
* C/C++: check "Plain Char is Signed"
* C/C++: add 'Include Paths' if desired: ..\..\include

```
#!c

#include <tm4c123gh6pm.h>
```

* Debug: add parameter [to Dialog DLL]: -dLaunchPadDLL
* Debug: for non-simulator use "Stellaris ICDI" [default is ULINK2/ME Cortext Debugger]
* Note: new main should include function SystemInit():

```
#!c

void SystemInit(void) {
}

```

* Initial files:


```
#!c

 -rwxrwxrwx  1 jkroub  wheel   192B Mar 20 06:26 ledOnBoard.build_log.htm
 -rwxrwxrwx  1 jkroub  wheel     0B Mar 20 06:24 ledOnBoard.uvproj
 -rwxrwxrwx  1 jkroub  wheel    30K Mar 21  2014 startup_TM4C123.s

```


### Other ###
IDE-Version:
µVision V4.74.0.22
Copyright (C) 2014 ARM Ltd and ARM Germany GmbH. All rights reserved.

License Information:
John Kroubalkian
UT.6.03x
LIC=----

* Tool Version Numbers:
* Toolchain:        MDK-Lite  Version: 4.74.0.0
* Toolchain Path:    C:\Keil\ARM\ARMCC\bin\
* C Compiler:         Armcc.Exe       V5.03.0.76 [Evaluation]
* Assembler:          Armasm.Exe       V5.03.0.76 [Evaluation]
* Linker/Locator:     ArmLink.Exe       V5.03.0.76 [Evaluation]
* Librarian:             ArmAr.Exe       V5.03.0.76 [Evaluation]
* Hex Converter:      FromElf.Exe       V5.03.0.76 [Evaluation]
* CPU DLL:               SARMCM3.DLL       V4.74.0.0
* Dialog DLL:         DCM.DLL       V1.10.0.0
* Target DLL:             lmidk-agdi.dll       V???
* Dialog DLL:         TCM.DLL       V1.14.1.0


### Target Device: TM4C123GH6PM ###

Old Part Number: LM4F230H5QR

ARM Cortex-M4F Processor Core
 - 80-MHz operation; 100 DMIPS performance
 - ARM Cortex SysTick Timer
 - Nested Vectored Interrupt Controller (NVIC)
 - Embedded Trace Macro and Trace Port
 - IEEE754-compliant single-precision floating-point unit

On-Chip Memory
 - 256 KB single-cycle Flash memory up to 40 MHz; a prefetch buffer improves performance above 40 MHz
 - 32 KB single-cycle SRAM
 - Internal ROM loaded with StellarisWare software:
 - 2KB EEPROM

Advanced Serial Integration
 - Two CAN 2.0 A/B controllers
 - USB 2.0 OTG/Host/Device
 - Eight UARTs with IrDA, 9-bit and ISO 7816 support (one UART with modem flow control)
 - Four I2C modules
 - Four Synchronous Serial Interface modules (SSI)

System Integration
 - Direct Memory Access Controller (DMA)
 - System control and clocks including on-chip precision 16-MHz oscillator
 - Six 32-bit timers (up to twelve 16-bit)
 - Six wide 64-bit timers (up to twelve 32-bit)
 - Twelve 16/32-bit Capture Compare PWM pins (CCP)
 - Twelve 32/64-bit Capture Compare PWM pins (CCP)
 - Lower-power battery-backed hibernation module
 - Real-Time Clock in Hibernation module
 - Two Watchdog Timers
 - Up to 43 GPIOs, depending on configuration

Advanced Motion Control
 - Two PWM modules, with a total of 16 advanced PWM outputs for motion and energy applications
 - Two fault inputs to promote low-latency shutdown
 - Two Quadrature Encoder Inputs (QEI)

Analog
 - Two 12-bit Analog-to-Digital Converters (ADC) with 12 analog input channels and a sample rate of one million samples/second
 - Two analog comparators
 - 16 digital comparators
 - On-chip voltage regulator

JTAG and ARM Serial Wire Debug (SWD)

64-pin LQFP package

Industrial (-40C to 85C) Temperature Range